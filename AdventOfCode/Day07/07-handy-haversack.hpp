//
//  07-handy-haversack.hpp
//  AdventOfCode
//
//  Created by swadi on 12/29/20.
//

#ifndef _7_handy_haversack_hpp
#define _7_handy_haversack_hpp

#include <string>
#include <set>
#include <map>

using namespace std;


class RecordItem {
public:
    RecordItem(string color, int count)
        : m_count(count), m_color(color)
    {
        
    }
    RecordItem(const RecordItem& other) {
        m_count = other.getCount();
        m_color = other.getColor();
    }
    
    string getColor() const { return m_color ;}
    int getCount() const { return m_count ;}
    bool operator<(const RecordItem& rhs) const
    {
        return m_count < rhs.getCount();
    }

private:
    RecordItem(); // Don't want default ctor
    int m_count;
    string m_color;
};

class Day07 {
public:
    Day07();
    bool loadData(const char* filename);
    void solve();
    unsigned long solveFirst();
    unsigned long solveSecond();

private:
    void init();
    unsigned long howManyColors(string color, set<string>& possibleParents);
    unsigned long howManyBags(string color, int indent);
    map<string, set<string>> m_data;
    int m_size;

    map<string, set<RecordItem>> m_hierarchy;
    int m_hierarchySize;
};

#endif /* _7_handy_haversack_hpp */
