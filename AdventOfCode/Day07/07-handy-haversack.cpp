//
//  07-handy-haversack.cpp
//  AdventOfCode
//
//  Created by swadi on 12/29/20.
//

#include "07-handy-haversack.hpp"

#include <iostream>
#include <fstream>
#include <regex>
#include <string>


Day07::Day07()
: m_size(0)
{
    
}

bool Day07::loadData(const char* filename)
{
    bool retval = true;
    ifstream file;
    string line;
    string key, value;
    std::regex regExpression("([a-z]+) ([a-z]+) bags contain (.*)");
    std::regex regExpChildren("(\\d) ([a-z]+) ([a-z]+) (bag[s]*)[,\\.]");
    std::smatch sm, smChild;
    string parent;
    
    m_data.clear();
    m_size = 0;
    m_hierarchy.clear();
    m_hierarchySize = 0;
    
    file.open(filename,ios::in);
    if (file.is_open()) {
        while (getline(file, line)) {
            std::regex_search(line, sm, regExpression);
            parent = string(sm[1]) + string(sm[2]);
            set<RecordItem> recordSet;
            m_hierarchy.insert(std::pair<string,set<RecordItem>>(parent,recordSet));
            string lastPart = sm[3];
            while ( std::regex_search(lastPart, smChild, regExpChildren) ) {
                string child = string(smChild[2]) + string(smChild[3]);
                int childCount = stoi(smChild[1]);
                if (m_data.count(child) == 0) {
                    set<string> emptySet;
                    m_data.insert(std::pair<string,set<string>>(child,emptySet));
                }
                set<string>& myset = m_data.at(child);
                myset.insert(parent);
                
                set<RecordItem>& myRecordSet = m_hierarchy.at(parent);
                myRecordSet.insert(RecordItem(child, childCount));
                
                lastPart = smChild.suffix().str();
            }
        }
    }

    m_size = m_data.size();
    m_hierarchySize = m_hierarchy.size();
    for (std::map<string,set<RecordItem>>::iterator it=m_hierarchy.begin(); it!=m_hierarchy.end(); ++it) {
        std::cout << it->first << " => [";
        for (std::set<RecordItem>::iterator setIter=it->second.begin(); setIter!=it->second.end(); ++setIter) {
            std::cout << "(" << setIter->getCount() << ") " << setIter->getColor() << ",";
        }
        cout << "]\n";
    }
    return retval;
}

unsigned long Day07::howManyColors(string color,set<string>& possibleParents)
{
    unsigned long retval = 0;
    set<string> parents;
    
    if (m_data.count(color) > 0) {
        parents = m_data.at(color);
        for (std::set<string>::iterator setIter=parents.begin(); setIter!=parents.end(); ++setIter) {
            if (possibleParents.count(*setIter) == 0) {
                possibleParents.insert(*setIter);
                howManyColors(*setIter, possibleParents);
            }
        }
    }
    
    return retval;
}

unsigned long Day07::howManyBags(string color, int indent)
{
    unsigned long retval = 0;
  
    if (m_hierarchy.count(color) > 0) {
        set<RecordItem>& children = m_hierarchy.at(color);
//        for (int j=0; j<indent; j++) cout << " ";
//        cout << color;
        for (std::set<RecordItem>::iterator setIter=children.begin(); setIter!=children.end(); ++setIter) {
//           cout << setIter->getColor() << " = " << setIter->getCount() << endl;
            string cColor = setIter->getColor();
            int howmany = setIter->getCount();
            retval += setIter->getCount() + setIter->getCount() * howManyBags(setIter->getColor(),indent+2);
//            cout << "[" << retval << "]" << endl;
        }
    }
    return retval;

}

unsigned long Day07::solveFirst()
{
    unsigned long retval = 0;
    set<string> possibleParents;
    howManyColors("shinygold", possibleParents);
    
    return possibleParents.size();
}

unsigned long Day07::solveSecond()
{
    unsigned long retval = 0;

//    retval = howManyBags("darkviolet");
//    retval = howManyBags("darkblue");
//    retval = howManyBags("darkgreen");

    retval = howManyBags("shinygold",0);
    
    return retval;
}

void Day07::solve()
{
    cout << "Day 7\n";
    loadData("/Users/swadi/work/davis/AdventOfCode/AdventOfCode/Day07/day07-sample.txt");
    
    cout << "Solution 1 (sample): " << solveFirst() <<endl ;
    cout << "Solution 2 (sample): " << solveSecond() << endl ;
    
    
    loadData("/Users/swadi/work/davis/AdventOfCode/AdventOfCode/Day07/day07-data.txt");
    
    cout << "Solution 1 : " << solveFirst() <<endl ;
    cout << "Solution 2 : " << solveSecond() << endl ;
    
    cout << "-----------------------------\n";
}

