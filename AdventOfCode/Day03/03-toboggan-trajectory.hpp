//
//  03-toboggan-trajectory.hpp
//  AdventOfCode
//
//  Created by swadi on 12/27/20.
//

#ifndef _3_toboggan_trajectory_hpp
#define _3_toboggan_trajectory_hpp

#include <vector>
#include <string>

using namespace std;

class Day03 {
public:
    Day03();
    bool loadData(const char* filename);
    void solve();
    unsigned long solveFirst();
    unsigned long solveSecond();

private:
    vector<string> m_data;
    int m_size;
};
#endif /* _3_toboggan_trajectory_hpp */
