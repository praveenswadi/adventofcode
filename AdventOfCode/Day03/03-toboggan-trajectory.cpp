//
//  03-toboggan-trajectory.cpp
//  AdventOfCode
//
//  Created by swadi on 12/27/20.
//

#include "03-toboggan-trajectory.hpp"

#include <iostream>
#include <fstream>

Day03::Day03()
{
    
}

bool Day03::loadData(const char* filename)
{
    bool retval = true;
    string line;
    ifstream file;
    
    m_data.clear();
    
    file.open(filename,ios::in);
    if (file.is_open()) {
        while (file >> line) {
            m_data.push_back(line);
        }
    }
   
    m_size = m_data.size();

    return retval;
}

unsigned long Day03::solveFirst()
{
    unsigned long retval = 0;

    int rows = m_size;
    int columns = m_data[0].size();
    int col = 0;
    
    for (int row = 1;row<rows;row++) {
        col = (col + 3) % columns;
        if (m_data[row][col] == '#')
            retval++;
    }
    return retval;
}

unsigned long Day03::solveSecond()
{
    unsigned long retval = 1;

    int rows = m_size;
    int columns = m_data[0].size();

    int col = 0;
    int noOfTrees = 0;
    int slopes[][2] = {
        {1,1},
        {3,1},
        {5,1},
        {7,1},
        {1,2}
    };
    
    int noOfCombos = sizeof(slopes)/sizeof(slopes[0]);
    
    for (int combo = 0; combo < noOfCombos; combo++) {
        for (int row = slopes[combo][1];row<rows;row = row+slopes[combo][1]) {
            col = (col + slopes[combo][0]) % columns;
            if (m_data[row][col] == '#')
                noOfTrees++;
        }
        cout << noOfTrees << endl;
        retval *= noOfTrees;
        col = 0;
        noOfTrees = 0;
    }
    return retval;
}

void Day03::solve()
{
    cout << "Day 3\n";
    loadData("/Users/swadi/work/davis/AdventOfCode/AdventOfCode/Day03/day03-sample.txt");
    
    cout << "Solution 1 (sample): " << solveFirst() <<endl ;
    cout << "Solution 2 (sample): " << solveSecond() << endl ;

    
    loadData("/Users/swadi/work/davis/AdventOfCode/AdventOfCode/Day03/day03-data.txt");
    
    cout << "Solution 1 : " << solveFirst() <<endl ;
    cout << "Solution 2 : " << solveSecond() << endl ;

    cout << "-----------------------------\n";
}
