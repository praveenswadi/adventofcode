//
//  01-report-repair.hpp
//  AdventOfCode
//
//  Created by swadi on 12/27/20.
//

#ifndef _1_report_repair_hpp
#define _1_report_repair_hpp

#include <vector>

using namespace std;

class Day01 {
public:
    Day01 ();
    bool loadData(const char* filename);
    void solve();
    unsigned long solveFirst(int sum=2020, int startIdx=0);
    unsigned long solveSecond();
    
private:
    bool getSecondNumberFor(int sum, int startIdx, int& otherIdx);

    vector<int> expenses;
    int m_size;
};

#endif /* _1_report_repair_hpp */
