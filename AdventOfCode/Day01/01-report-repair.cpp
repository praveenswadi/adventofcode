//
//  01-report-repair.cpp
//  AdventOfCode
//
//  Created by swadi on 12/27/20.
//

#include "01-report-repair.hpp"
#include <iostream>
#include <fstream>
#include <algorithm>

Day01::Day01()
{
    
}

bool Day01::loadData(const char* filename)
{
    bool retval = true;
    int number;
    ifstream file;
    
    expenses.clear();
    
    file.open(filename,ios::in);
    if (file.is_open()) {
        while (file >> number) {
            expenses.push_back(number);
        }
    }
   
    m_size = expenses.size();
    sort(expenses.begin(),expenses.end());
//    for (int i=0; i< m_size; i++) {
//        cout << expenses[i] << endl;
//    }
    return retval;
}

bool Day01::getSecondNumberFor(int remaining, int startIdx, int& otherIndex)
{
    bool found = false;
    int next;

    for (int j=startIdx; j<m_size; j++) {
        next = expenses[j];
        if (next == remaining) {
            found = true;
            otherIndex = j;
            break;
        } else if (next > remaining) {
            break;
        }
    }

    
    return found;
}

unsigned long Day01::solveFirst(int sum, int startIdx)
{
    int remaining, nextIdx;
    unsigned long product = 0;
    
    for (int i=startIdx;i<m_size-1;i++) {
        product = expenses[i];
        remaining = sum - product;
        if (getSecondNumberFor(remaining, i+1, nextIdx)) {
            return product * expenses[nextIdx];
        }
    }
    return 0;
}


unsigned long Day01::solveSecond()
{
    unsigned long product = 0;
    int current;
    
    for (int i=0; i<m_size-1; i++) {
        current = expenses[i];
        product = solveFirst(2020-current,i);
        if (product != 0)
            return current * product;
    }

    return 0;
}

void Day01::solve()
{
    cout << "Day 1\n";
    loadData("/Users/swadi/work/davis/AdventOfCode/AdventOfCode/Day01/day01-sample.txt");
    
    cout << "Solution 1 (sample): " << solveFirst() <<endl ;
    cout << "Solution 2 (sample): " << solveSecond() << endl ;

    loadData("/Users/swadi/work/davis/AdventOfCode/AdventOfCode/Day01/day01-data.txt");
    
    cout << "Solution 1 : " << solveFirst() <<endl ;
    cout << "Solution 2 : " << solveSecond() << endl ;

    cout << "-----------------------------\n";
}
