//
//  all-days.hpp
//  AdventOfCode
//
//  Created by swadi on 12/27/20.
//

#ifndef all_days_h
#define all_days_h

#include "01-report-repair.hpp"
#include "02-password-philosophy.hpp"
#include "03-toboggan-trajectory.hpp"
#include "04-passport-processing.hpp"
#include "05-binary-boarding.hpp"
#include "06-custom-customs.hpp"
#include "07-handy-haversack.hpp"



#endif /* all_days_h */
