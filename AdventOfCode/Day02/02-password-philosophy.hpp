//
//  02-password-philosophy.hpp
//  AdventOfCode
//
//  Created by swadi on 12/27/20.
//

#ifndef _2_password_philosophy_hpp
#define _2_password_philosophy_hpp

#include <vector>
#include <string>

using namespace std;
struct Item {
public:
    Item(int min_, int max_, char ch_, string pass);
    int min, max;
    char ch;
    string password;
};

class Day02 {
public:
    Day02();
    bool loadData(const char* filename);
    void solve();
    unsigned long solveFirst();
    unsigned long solveSecond();

private:
    vector<Item> m_data;
    int m_size;
};


#endif /* _2_password_philosophy_hpp */
