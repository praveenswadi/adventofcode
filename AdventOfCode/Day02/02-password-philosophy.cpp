//
//  02-password-philosophy.cpp
//  AdventOfCode
//
//  Created by swadi on 12/27/20.
//

#include "02-password-philosophy.hpp"

#include <iostream>
#include <fstream>


Item::Item(int min_, int max_, char ch_, string pass)
: min(min_), max(max_), ch(ch_), password(pass)
{
    
}


Day02::Day02()
{
    
}

bool Day02::loadData(const char* filename)
{
    bool retval = true;
    string policy, charToCheck, password;
    ifstream file;
    int pos, min, max;
    
    m_data.clear();
    
    file.open(filename,ios::in);
    if (file.is_open()) {
        while (file >> policy >> charToCheck >> password) {
            pos = policy.find("-");
            min = stoi(policy.substr(0,pos));
            max = stoi(policy.substr(pos+1));
            char ch = charToCheck[0];
            m_data.push_back(Item(min,max,ch,password));
        }
    }
   
    m_size = m_data.size();

    return retval;
}

unsigned long Day02::solveFirst()
{
    unsigned long retval = 0;
    
    for (int i=0;i<m_size;i++) {
        int s = 0;
        const char* str = m_data[i].password.c_str();
        int size = m_data[i].password.length();
        for (int j=0;j<size;j++) {
            if (str[j] == m_data[i].ch) {
              s++;
            }
        }
        
        if (s >= m_data[i].min && s <= m_data[i].max)
            retval++;
    }
    
    return retval;
}

unsigned long Day02::solveSecond()
{
    unsigned long retval = 0;
 
    for (int i=0;i<m_size;i++) {
        char minChar, maxChar;
        const string& s = m_data[i].password;
        minChar = s.at(m_data[i].min - 1);
        maxChar = s.at(m_data[i].max - 1);
        if ( (minChar == m_data[i].ch) ^ (maxChar == m_data[i].ch))
            retval++;
    }

    return retval;
}

void Day02::solve()
{
    cout << "Day 2\n";
    loadData("/Users/swadi/work/davis/AdventOfCode/AdventOfCode/Day02/day02-sample.txt");
    
    cout << "Solution 1 (sample): " << solveFirst() <<endl ;
    cout << "Solution 2 (sample): " << solveSecond() << endl ;

    
    loadData("/Users/swadi/work/davis/AdventOfCode/AdventOfCode/Day02/day02-data.txt");
    
    cout << "Solution 1 : " << solveFirst() <<endl ;
    cout << "Solution 2 : " << solveSecond() << endl ;

    cout << "-----------------------------\n";
}
