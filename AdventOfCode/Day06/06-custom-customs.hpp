//
//  06-custom-customs.hpp
//  AdventOfCode
//
//  Created by swadi on 12/29/20.
//

#ifndef _6_custom_customs_hpp
#define _6_custom_customs_hpp

#include <string>
#include <vector>

using namespace std;

class Day06 {
public:
    Day06();
    bool loadData(const char* filename);
    void solve();
    unsigned long solveFirst();
    unsigned long solveSecond();

private:
    void init();
    vector<int> m_data;
    int m_size;
    int m_solution2;

};


#endif /* _6_custom_customs_hpp */
