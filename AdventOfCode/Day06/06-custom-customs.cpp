//
//  06-custom-customs.cpp
//  AdventOfCode
//
//  Created by swadi on 12/29/20.
//

#include "06-custom-customs.hpp"

#include <iostream>
#include <fstream>
#include <regex>
#include <string>


Day06::Day06()
: m_size(0)
{
    
}

void Day06::init()
{
    m_data.clear();
    m_size = 0;
    m_solution2 = 0;

}

bool Day06::loadData(const char* filename)
{
    bool retval = true;
    ifstream file;
    string line;
    bool itemCompleted = false;
    std::regex regExpression("([a-z]+):([a-z0-9#]+)");
    std::smatch sm;
    int count = 0;
    int peopleInGroup = 0;
    int answers[26];
    init();
    
    for (int i=0;i<26;i++) answers[i] = 0;
    file.open(filename,ios::in);
    if (file.is_open()) {
        while (getline(file, line)) {
            if (line.length() == 0) {
                itemCompleted = true;
                // calculate count
                count = 0;
                for (int i=0;i<26;i++) {
                    if (answers[i] > 0) count++;
                    if (answers[i] == peopleInGroup) m_solution2++;
                    answers[i] = 0;
                }
                m_data.push_back(count);
                peopleInGroup = 0;
            } else {
                itemCompleted = false;
                peopleInGroup++;
                int length = line.length();
                for (int i=0;i<length;i++) {
                    char ch = line[i];
                    int idx = ch - 97;
                    answers[idx]++;
                }
            }
        }
    }
    
    m_size = m_data.size();
    
    return retval;
}

unsigned long Day06::solveFirst()
{
    unsigned long retval = 0;
    
    for (int i=0;i<m_size;i++)
        retval += m_data[i];
    
    return retval;
}

unsigned long Day06::solveSecond()
{
    return m_solution2;
}

void Day06::solve()
{
    cout << "Day 6\n";
    loadData("/Users/swadi/work/davis/AdventOfCode/AdventOfCode/Day06/day06-sample.txt");
    
    cout << "Solution 1 (sample): " << solveFirst() <<endl ;
    cout << "Solution 2 (sample): " << solveSecond() << endl ;
    
    
    loadData("/Users/swadi/work/davis/AdventOfCode/AdventOfCode/Day06/day06-data.txt");
    
    cout << "Solution 1 : " << solveFirst() <<endl ;
    cout << "Solution 2 : " << solveSecond() << endl ;
    
    cout << "-----------------------------\n";
}
