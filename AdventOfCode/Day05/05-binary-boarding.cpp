//
//  04-binary-boarding.cpp
//  AdventOfCode
//
//  Created by swadi on 12/28/20.
//

#include "05-binary-boarding.hpp"

#include <iostream>
#include <fstream>

void Day05::init()
{
    for (int i=0; i<1024; i++) {
            m_seatTaken[i] = false;
    }
    m_data.clear();
    m_size = m_data.size();
    m_highestSeatId = 0;
}

Day05::Day05()
{
    init();
}

bool Day05::loadData(const char* filename)
{
    bool retval = true;
    string line;
    ifstream file;
    
    init();

    file.open(filename,ios::in);
    if (file.is_open()) {
        while (file >> line) {
            m_data.push_back(line);
        }
    }
   
    m_size = m_data.size();

    return retval;
}

unsigned long Day05::solveFirst()
{
    unsigned long retval = 0;
    int seat;
    
    for (int i=0; i<m_size; i++) {
        int rowLow =0, rowHigh = 127;
        int colLow = 0, colHigh = 7;
        int row = 0, col = 0;

        int size = m_data[i].size();
        for (int j=0;j<size;j++) {
            row = (rowHigh + rowLow)/2;
            col = (colHigh + colLow) / 2;
            char ch = m_data[i][j];
            if (ch == 'F') {
                rowHigh = row;
            } else if (ch == 'B') {
                rowLow = ++row;
            } else if (ch == 'R') {
                colLow = ++col;
            } else if (ch == 'L') {
                colHigh = col;
            } else {
                cout << "WTF?\n";
            }
        }
        seat = row * 8 + col;
        m_seatTaken[seat] = true;
        if (seat > retval)
            retval = seat;
    }

    m_highestSeatId = retval;
    return retval;
}

unsigned long Day05::solveSecond()
{
    int seatId = 0;
    while (!m_seatTaken[seatId++]);
    while (m_seatTaken[seatId++]);

    return seatId-1;
}

void Day05::solve()
{
    cout << "Day 5\n";
    loadData("/Users/swadi/work/davis/AdventOfCode/AdventOfCode/Day05/day05-sample.txt");
    
    cout << "Solution 1 (sample): " << solveFirst() <<endl ;
    cout << "Solution 2 (sample): " << solveSecond() << endl ;

    
    loadData("/Users/swadi/work/davis/AdventOfCode/AdventOfCode/Day05/day05-data.txt");
    
    cout << "Solution 1 : " << solveFirst() <<endl ;
    cout << "Solution 2 : " << solveSecond() << endl ;

    cout << "-----------------------------\n";
}
