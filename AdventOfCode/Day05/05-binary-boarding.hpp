//
//  04-binary-boarding.hpp
//  AdventOfCode
//
//  Created by swadi on 12/28/20.
//

#ifndef _5_binary_boarding_hpp
#define _5_binary_boarding_hpp

#include <vector>
#include <string>

using namespace std;

class Day05 {
public:
    Day05();
    bool loadData(const char* filename);
    void solve();
    unsigned long solveFirst();
    unsigned long solveSecond();

private:
    void init();
    vector<string> m_data;
    int m_size;
    bool m_seatTaken[1024];
    int m_highestSeatId;
};

#endif /* _4_binary_boarding_hpp */
