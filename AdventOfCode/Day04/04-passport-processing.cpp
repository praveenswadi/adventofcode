//
//  04-passport-processing.cpp
//  AdventOfCode
//
//  Created by swadi on 12/27/20.
//

#include "04-passport-processing.hpp"

#include <iostream>
#include <fstream>
#include <regex>
#include <string>

PassportItem::PassportItem()
: isValid(false)
{
    
}

PassportItem::PassportItem(const PassportItem& other)
{
    std::map<string,string> otherFields = other.fields;
    
    std::map<string,string>::iterator it = otherFields.begin();
    
    for (it = otherFields.begin(); it!=otherFields.end(); ++it) {
        insert(it->first, it->second);
    }
    isValid = other.isValid;
}


void PassportItem::insert(string key, string val)
{
    bool okayToAdd = false;
    
    if (key.compare("byr") == 0) {
        int byr = stoi(val);
        if (byr >= 1920 && byr <= 2002)
            okayToAdd = true;
    } else if (key.compare("iyr") == 0) {
        int iyr = stoi(val);
        if (iyr >= 2010 && iyr <= 2020)
            okayToAdd = true;
    } else if (key.compare("eyr") == 0) {
        int eyr = stoi(val);
        if (eyr >= 2020 && eyr <= 2030)
            okayToAdd = true;
    } else if (key.compare("hgt") == 0) {
        if (val.length() > 3) {
            string hgtSubstr = val.substr(0,val.length()-2);
            string hgtMetric = val.substr(val.length()-2);
            int height = stoi(hgtSubstr);
            if ((hgtMetric.compare("in") == 0) &&
                height >= 59 && height <= 76) {
                okayToAdd = true;
            } else if ((hgtMetric.compare("cm") == 0) &&
                       height >= 150 && height <= 193) {
                okayToAdd = true;
            }
        }
    } else if (key.compare("hcl") == 0) {
        std::regex rcolor("#[0-9a-f][0-9a-f][0-9a-f][0-9a-f][0-9a-f][0-9a-f]");
        okayToAdd = regex_match(val, rcolor);
    } else if (key.compare("ecl") == 0) {
        std::regex rEyeColor("(amb)|(blu)|(brn)|(gry)|(grn)|(hzl)|(oth)");
        okayToAdd = regex_match(val, rEyeColor);
        
    } else if (key.compare("pid") == 0) {
        std::regex pid("[\\d]{9}");
        okayToAdd = regex_match(val, pid);
    } else if (key.compare("cid") == 0) {
        okayToAdd = true;
    }
    
    if (okayToAdd)
        fields.insert( std::pair<string,string>(key,val) );
}


bool PassportItem::setValidity()
{
    unsigned long size = fields.size();
    if (size == 8) {
        isValid = true;
    } else if (size == 7 && fields.count("cid") == 0 ) {
        isValid = true;
    }
    return isValid;
}

Day04::Day04()
: m_size(0), m_validPassports(0)
{
    
}

bool Day04::loadData(const char* filename)
{
    bool retval = true;
    string policy, charToCheck, password;
    ifstream file;
    string line, keyval;
    string key, value;
    PassportItem *pItem;
    bool itemCompleted = false;
    std::regex regExpression("([a-z]+):([a-z0-9#]+)");
    std::smatch sm;
    
    m_data.clear();
    m_size = 0;
    m_validPassports = 0;
    
    pItem = new PassportItem();
    file.open(filename,ios::in);
    if (file.is_open()) {
        while (getline(file, line)) {
            if (line.length() == 0) {
                // item complete
                itemCompleted = true;
                if (pItem->setValidity())
                    m_validPassports++;
                m_data.push_back(*pItem);
                delete pItem;
                pItem = new PassportItem();
            } else {
                itemCompleted = false;
                
                while ( std::regex_search(line, sm, regExpression) ) {
                    pItem->insert(sm[1], sm[2]);
                    line = sm.suffix().str();
                }
                
            }
        }
    }
    
    if (!itemCompleted) {
        m_data.push_back(*pItem);
    }
    delete pItem;
    m_size = m_data.size();
    
    return retval;
}

unsigned long Day04::solveFirst()
{
    unsigned long retval = 0;
    
    
    return m_validPassports;
}

unsigned long Day04::solveSecond()
{
    unsigned long retval = 0;
    
    
    return retval;
}

void Day04::solve()
{
    cout << "Day 4\n";
    loadData("/Users/swadi/work/davis/AdventOfCode/AdventOfCode/Day04/day04-sample.txt");
    
    cout << "Solution 1 (sample): " << solveFirst() <<endl ;
    cout << "Solution 2 (sample): " << solveSecond() << endl ;
    
    
    loadData("/Users/swadi/work/davis/AdventOfCode/AdventOfCode/Day04/day04-data.txt");
    
    cout << "Solution 1 : " << solveFirst() <<endl ;
    cout << "Solution 2 : " << solveSecond() << endl ;
    
    cout << "-----------------------------\n";
}
