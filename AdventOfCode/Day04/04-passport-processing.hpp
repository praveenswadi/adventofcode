//
//  04-passport-processing.hpp
//  AdventOfCode
//
//  Created by swadi on 12/27/20.
//

#ifndef _4_passport_processing_hpp
#define _4_passport_processing_hpp

#include <string>
#include <vector>
#include <map>

using namespace std;

class PassportItem {
public:
    PassportItem();
    PassportItem(const PassportItem& other);

    void insert(string key, string val);
    bool setValidity();
    
    std::map<string, string> fields;
    bool isValid;
};

class Day04 {
public:
    Day04();
    bool loadData(const char* filename);
    void solve();
    unsigned long solveFirst();
    unsigned long solveSecond();

private:
    vector<PassportItem> m_data;
    int m_size;
    int m_validPassports;
};


#endif /* _4_passport_processing_hpp */
