//
//  main.cpp
//  AdventOfCode
//
//  Created by swadi on 12/27/20.
//

#include <iostream>
#include "all-days.hpp"

int main(int argc, const char * argv[]) {
    Day01 day1;
    Day02 day2;
    Day03 day3;
    Day04 day4;
    Day05 day5;
    Day06 day6;
    Day07 day7;
    
    //day1.solve();
    //day2.solve();
    //day3.solve();
    //day4.solve();
    day7.solve();
    
    
    return 0;
}
